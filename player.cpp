#include "player.h"
#include <algorithm>
#include <climits>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    othello = new Board();
    this->side = side;
    other = (side == BLACK) ? WHITE : BLACK;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete othello;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    othello->doMove(opponentsMove, other);


    if (!othello->hasMoves(side)) {
        return NULL;
    }

    if (testingMinimax) {
        return doMinimaxMove();
    }
    // depending on how much time I have left, use different depths
    else {
        if(msLeft == -1 || msLeft > 5000) {
            return doAlphaBetaMove(5);
        }
        else if (msLeft > 3000) {
            return doAlphaBetaMove(3);
        }
        else {
            return doAlphaBetaMove(1);
        }
        
    }

    return NULL;
}

/*
 * Computes the next move using a "depth"-ply minimax algorithm to minimize
 * loss of points. Depth depends on how much time I have left to make a move.
 */
Move *Player::doAlphaBetaMove(int depth) {
    int maxMoves = -INT_MAX, candidate;
    Move *abMove = new Move(-1, -1);
    Move *candMove;
    Board *candBoard;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            candMove = new Move(i, j);
            
            // if the move is valid
            if (othello->checkMove(candMove, side)) {

                // make a copy of the board and perform the move on it
                candBoard = othello->copy();
                candBoard->doMove(candMove, side);

                // run the alpha beta pruning minimax algorithm on this move
                candidate = alphaBeta(candBoard, depth, -INT_MAX, INT_MAX, false);

                // find the move with the best outcome
                if (candidate > maxMoves) {
                    maxMoves = candidate;
                    abMove->setX(i);
                    abMove->setY(j);
                }
                delete candBoard;
            }
            delete candMove;
        }
    }

    // update internal board state
    othello->doMove(abMove, side);

    return abMove;
}

/*
 * Performs minimax with alpha beta pruning recursively on the valid moves
 * in the passed in board. It stops evaluating and return early if alpha > beta.
 */
int Player::alphaBeta(Board *board, int depth, int alpha, int beta, bool player) {
    int candidate = 65;
    int cost = (player) ? -64 : 64;
    Move *candMove;
    Board *candBoard;
    Side turn;

    (player) ? turn = side : turn = other;

    // base case: return how many more tiles you have vs your opponent
    if (depth == 0) {
        return board->count(side) - board->count(other);
    }

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            candMove = new Move(i, j);

            // if move is valid
            if (board->checkMove(candMove, turn)) {

                // make a copy of the board and perform the move on it
                candBoard = board->copy();
                candBoard->doMove(candMove, turn);

                // recursively call alpha beta pruning minimax to get best move.
                candidate = alphaBeta(candBoard, depth - 1, alpha, beta, !player);

                // if it's the player's turn, we can to choose the most
                // favorable move -> highest score.
                if (player) {
                    if (candidate > cost) {
                        cost = candidate;
                    }
                    // update alpha and check if branch should be pruned
                    if(cost > alpha) {
                        alpha = cost;
                    }
                    if (alpha > beta) {
                        break;
                    }
                }
                // otherwise we choose the lowest score for the worst
                // case scenario
                else {
                    if (candidate < cost) {
                        cost = candidate;
                    }
                    // update beta and check if branch should be pruned
                    if (cost < beta) {
                        beta = cost;
                    }
                    if (beta < alpha) {
                        break;
                    }
                }
                delete candBoard;
            }
            delete candMove;
        }
    }

    // no children found
    if (candidate == 65) {
        return board->count(side) - board->count(other);
    }

    return cost;
}

/*
 * Computes the next move by calculating the move that will flip the
 * most tiles.
 */
Move *Player::doMaxMove() {

    int maxFlip = 0, candidate;
    Move *maxMove = new Move(-1, -1);
    Move *move;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {

            move = new Move(i, j);

            // if move is valid
            if (othello->checkMove(move, side)) {

                // get how many tiles it will flip
                candidate = getFlank(move, side);
                
                // find the max number of tiles flipped
                if (candidate > maxFlip) {
                    maxFlip = candidate;
                    maxMove->setX(i);
                    maxMove->setY(j);
                }

            }
            delete move;
        }
    }

    // update the internal board state
    othello->doMove(maxMove, side);

    return maxMove;
}

/*
 * Computes the next move using a 2-ply minimax algorithm to minimize
 * loss of points.
 */
Move *Player::doMinimaxMove() {

    int minCost = -64, candidate;
    Move *minMove = new Move(-1, -1);
    Move *candMove;
    Board *candBoard;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            candMove = new Move(i, j);
            
            // if the move is valid
            if (othello->checkMove(candMove, side)) {

                // make a copy of the board and perform the move on it
                candBoard = othello->copy();
                candBoard->doMove(candMove, side);

                // run the minimax algorithm on this move
                candidate = minimax(candBoard, 1, false);

                // find the move with the best outcome
                if (candidate > minCost) {
                    minCost = candidate;
                    minMove->setX(i);
                    minMove->setY(j);
                }
                delete candBoard;
            }
            delete candMove;
        }
    }

    // update internal board state
    othello->doMove(minMove, side);

    return minMove;
}

/*
 * Performs minimax recursively on all valid moves in the pass in
 * board. If the move is this player's move, then it returns the highest
 * score from all possible moves. Otherwise, it finds the lowest score
 * of all the possible moves. This takes into account the worst case
 * scenario if the opponent plays perfectly.
 */
int Player::minimax(Board *board, int depth, bool player) {
    int cost = (player) ? -64 : 64;
    int candidate;
    Move *candMove;
    Board *candBoard;
    Side turn;

    (player) ? turn = side : turn = other;

    // base case: return how many more tiles you have vs your opponent
    if (depth == 0) {
        return board->count(side) - board->count(other);
    }

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            candMove = new Move(i, j);

            // if move is valid
            if (board->checkMove(candMove, turn)) {

                // make a copy of the board and perform the move on it
                candBoard = board->copy();
                candBoard->doMove(candMove, turn);

                // recursively call minimax to get best move.
                candidate = minimax(candBoard, depth - 1, !player);

                // if it's the player's turn next, we can to choose the most
                // favorable move -> highest score.
                if (player) {
                    if (candidate > cost) {
                        cost = candidate;
                    }
                }
                // otherwise we choose the lowest score for the worst
                // case scenario
                else {
                    if (candidate < cost) {
                        cost = candidate;
                    }
                }
                delete candBoard;
            }
            delete candMove;
        }
    }

    return cost;
}

/*
 * Calculates how many tiles would be flipped for a given move.
 */
int Player::getFlank(Move *move, Side turn) {
    int hori, vert, forward, back;
    hori = getHorizontalFlank(move, turn);
    vert = getVerticalFlank(move, turn);
    forward = getForDiagFlank(move, turn);
    back = getBackDiagFlank(move, turn);

    return hori + vert + forward + back;
}

/*
 * Calculates how many tiles would be flipped in the horizontal direction
 */
int Player::getHorizontalFlank(Move *move, Side turn) {
    int xpos = move->getX() + 1;
    int ypos = move->getY();
    int flips = 0, candFlips = 0;
    Side piece;

    // move right from new piece
    while (xpos < 8) {
        if (othello->getPiece(xpos, ypos) == 0) {
            piece = WHITE;
        }
        else if (othello->getPiece(xpos, ypos) == 1) {
            piece = BLACK;
        }
        else {
            break; // if it's an empty cell, stop looking
        }

        // stones counted by candFlips are bounded by turn's tile
        if (piece == turn) {
            flips = candFlips;
        }
        // counting continguous tiles of opposite color
        else {
            candFlips++;
        }

        xpos++;
    }

    xpos = move->getX() - 1;
    candFlips = flips;

    // move left from new piece
    while (xpos >= 0) {
        if (othello->getPiece(xpos, ypos) == 0) {
            piece = WHITE;
        }
        else if (othello->getPiece(xpos, ypos) == 1) {
            piece = BLACK;
        }
        else {
            return flips;
        }
        // stones counted by candFlips are bounded by turn's tile
        if (piece == turn) {
            flips = candFlips;
        }
        // counting continguous tiles of opposite color
        else {
            candFlips++;
        }

        xpos--;
    }

    return flips;
}

int Player::getVerticalFlank(Move *move, Side turn) {
    int xpos = move->getX();
    int ypos = move->getY() - 1;
    int flips = 0, candFlips = 0;
    Side piece;

    while (ypos >= 0) {
        if (othello->getPiece(xpos, ypos) == 0) {
            piece = WHITE;
        }
        else if (othello->getPiece(xpos, ypos) == 1) {
            piece = BLACK;
        }
        else {
            break;
        }

        // stones counted by candFlips are bounded by turn's tile
        if (piece == turn) {
            flips = candFlips;
        }
        // counting continguous tiles of opposite color
        else {
            candFlips++;
        }

        ypos--;
    }

    ypos = move->getY() + 1;
    candFlips = flips;

    while (ypos < 8) {
        if (othello->getPiece(xpos, ypos) == 0) {
            piece = WHITE;
        }
        else if (othello->getPiece(xpos, ypos) == 1) {
            piece = BLACK;
        }
        else {
            return flips;
        }

        // stones counted by candFlips are bounded by turn's tile
        if (piece == turn) {
            flips = candFlips;
        }
        // counting continguous tiles of opposite color
        else {
            candFlips++;
        }

        ypos++;
    }

    return flips;
}

int Player::getForDiagFlank(Move *move, Side turn) {
    int xpos = move->getX() + 1;
    int ypos = move->getY() - 1;
    int flips = 0, candFlips = 0;
    Side piece;

    while (ypos >= 0 && xpos < 8) {
        if (othello->getPiece(xpos, ypos) == 0) {
            piece = WHITE;
        }
        else if (othello->getPiece(xpos, ypos) == 1) {
            piece = BLACK;
        }
        else {
            break;
        }

        // stones counted by candFlips are bounded by turn's tile
        if (piece == turn) {
            flips = candFlips;
        }
        // counting continguous tiles of opposite color
        else {
            candFlips++;
        }

        ypos--;
        xpos++;
    }

    ypos = move->getY() + 1;
    xpos = move->getX() - 1;
    candFlips = flips;

    while (ypos < 8 && xpos >= 0) {
        if (othello->getPiece(xpos, ypos) == 0) {
            piece = WHITE;
        }
        else if (othello->getPiece(xpos, ypos) == 1) {
            piece = BLACK;
        }
        else {
            return flips;
        }

        // stones counted by candFlips are bounded by turn's tile
        if (piece == turn) {
            flips = candFlips;
        }
        // counting continguous tiles of opposite color
        else {
            candFlips++;
        }

        ypos++;
        xpos--;
    }

    return flips;
}

int Player::getBackDiagFlank(Move *move, Side turn) {
    int xpos = move->getX() - 1;
    int ypos = move->getY() - 1;
    int flips = 0, candFlips = 0;
    Side piece;

    while (ypos >= 0 && xpos >= 0) {
        if (othello->getPiece(xpos, ypos) == 0) {
            piece = WHITE;
        }
        else if (othello->getPiece(xpos, ypos) == 1) {
            piece = BLACK;
        }
        else {
            break;
        }

        // stones counted by candFlips are bounded by turn's tile
        if (piece == turn) {
            flips = candFlips;
        }
        // counting continguous tiles of opposite color
        else {
            candFlips++;
        }

        ypos--;
        xpos--;
    }

    ypos = move->getY() + 1;
    xpos = move->getX() + 1;
    candFlips = flips;

    while (ypos < 8 && xpos < 8) {
        if (othello->getPiece(xpos, ypos) == 0) {
            piece = WHITE;
        }
        else if (othello->getPiece(xpos, ypos) == 1) {
            piece = BLACK;
        }
        else {
            return flips;
        }

        // stones counted by candFlips are bounded by turn's tile
        if (piece == turn) {
            flips = candFlips;
        }
        // counting continguous tiles of opposite color
        else {
            candFlips++;
        }

        ypos++;
        xpos++;
    }

    return flips;
}

/*
 * Function used to set internal player board from outside
 */
void Player::setBoard(char data[]) {
    othello->setBoard(data);
}
