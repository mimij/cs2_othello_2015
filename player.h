#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

private:
	Board *othello;
	Side side;
	Side other;

	Move *doMaxMove();
    Move *doMinimaxMove();
    Move *doAlphaBetaMove(int depth);
    int minimax(Board *board, int depth, bool player);
    int alphaBeta(Board *board, int depth, int alpha, int beta, bool player);
    int getFlank(Move *move, Side turn);
    int getHorizontalFlank(Move *move, Side turn);
    int getVerticalFlank(Move *move, Side turn);
    int getForDiagFlank(Move *move, Side turn);
    int getBackDiagFlank(Move *move, Side turn);

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    void setBoard(char data[]);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
