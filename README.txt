I did this assignment alone.
My first algorithm merely looked at every move and did the move that would flip the most tiles, without taking into account future moves or the moves the opponent would make. This was slightly better than the random player.
Then I did a 2-ply minimax. This finds the move that minimizes the possible number of points lost in the next few moves. This won against the simple player nearly every time.
I added alpha beta pruning to my minimax algorithm. It now also takes into account the time left to play its move.
If there is unlimited time or more than 5 seconds, it will do a 6 ply minimax with alpha beta pruning.
Otherwise, it does the minimax with fewer levels.
